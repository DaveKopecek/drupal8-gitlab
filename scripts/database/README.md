Dockerfile to build a MariaDB image which contains the project's database.

# Description

This Dockerfile creates and populates a database with the name drupal8. 

# Building the image for the first time

## Dump the Drupal 8 database
Create a database dump of the database and save it to `scripts/database/dumps/drupal8.sql`.

## Create a repository at Quay.io

Create a repository at Quay.io via the web interface. There is no need to link the repository to a GitHub trigger.
Just create the repository.

## Authenticate, build, and push the image

```
cd scripts-database
docker build --tag quay.io/juampynr/drupal8ci:master
docker push quay.io/juampynr/drupal8ci:master
```

## Try pulling the image

Once the tag is available at Quay.io (monitor the web interface to know this), then pull the image locally
and test that it has the databases. Here is an example where I am starting a container at port 3307 (3306
is the default MySQL port but I already have MySQL running at the host machine):

```bash
docker pull quay.io/juampynr/drupal8_migrate:master
docker run -d --name drupal8_migrate -p 3307:3306 quay.io/juampynr/drupal8_migrate:master
mysql -h127.0.0.1 --port=3307 -uroot -p -e 'show databases;'
Enter password: 
+--------------------+
| Database           |
+--------------------+
| drupal7            |
| drupal8            |
| drupal8_config     |
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
```

Next, adjust the image at `.circleci/config.yml` so CircleCI will use it for the migration job.
